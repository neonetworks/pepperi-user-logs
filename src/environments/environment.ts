// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCztD0AtJkeS34yvDF3a0cMf4L3rbG7TH0',
    authDomain: 'neo-pepperi-userlogs.firebaseapp.com',
    databaseURL: 'https://neo-pepperi-userlogs.firebaseio.com',
    projectId: 'neo-pepperi-userlogs',
    storageBucket: '',
    messagingSenderId: '829414925078',
    appId: '1:829414925078:web:6fc384959ba9bb35322637'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
