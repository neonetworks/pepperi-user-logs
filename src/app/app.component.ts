import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PepperiUserLogs';

  token = null;

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  masterSelected: boolean;
  usersList = [];
  usersListLogs = {};
  usersSelectedList = [];

  isLoading = false;

  constructor(private http: HttpClient) {
    this.masterSelected = false;
  }

  initLoading() {
    this.isLoading = true;
  }

  stopLoading() {
    this.isLoading = false;
  }

  onLogin() {
    this.initLoading();
    const url = 'https://api.pepperi.com/v1.0/company/apitoken';
    const auth = btoa(`${this.loginForm.value.email}:${this.loginForm.value.password}`);
    const options = {
      headers: {
        Authorization: `Basic ${auth}`,
        'X-Pepperi-ConsumerKey': 'HoYFFbhWhddxJrLAs48rqKtR5t7f47cE'
      }
    };

    this.http.get(url, options).subscribe((res: any) => {
      console.log(res);
      if (res && res.APIToken) {
        this.token = res.APIToken;
      }
      this.stopLoading();
    }, error => {
      this.stopLoading();
    });
  }

  getUsers() {
    this.initLoading();
    const url = 'https://api.pepperi.com/v1.0/users';
    const auth = btoa(`TokenAuth:${this.token}`);
    const options = {
      headers: {
        Authorization: `Basic ${auth}`,
        'X-Pepperi-ConsumerKey': 'HoYFFbhWhddxJrLAs48rqKtR5t7f47cE'
      }
    };

    this.http.get(url, options).subscribe((res: any) => {
      console.log(res);
      if (res && res.length > 0) {
        this.usersList = res;
      }
      this.stopLoading();
    }, error => {
      this.stopLoading();
    });
  }

  getUsersLogs() {
    const url = 'https://api.pepperi.com/v1.0/users/';
    const auth = btoa(`TokenAuth:${this.token}`);

    const allReqs = [];

    console.log(this.usersSelectedList);

    if (this.usersSelectedList.length > 0) {
      this.initLoading();

      this.usersSelectedList.forEach(user => {
        const id = user.InternalID;
        // const id = this.usersList[0].InternalID;

        const options = {
          params: {
            page_size: '10'
          },
          headers: {
            Authorization: `Basic ${auth}`,
            'X-Pepperi-ConsumerKey': 'HoYFFbhWhddxJrLAs48rqKtR5t7f47cE'
          }
        };

        const resp = this.http.get(`${url}${id}/log`, options);
        allReqs.push(resp);
      });

      forkJoin(allReqs).subscribe(res => {
        this.usersSelectedList.forEach((user, idx) => {
          this.usersListLogs[user.InternalID] = res[idx];
        });

        console.log(this.usersListLogs);
        this.stopLoading();
      }, error => {
        this.stopLoading();
      });
    }
  }

  checkUncheckAll() {
    for (var i = 0; i < this.usersList.length; i++) {
      this.usersList[i].isSelected = this.masterSelected;
    }    
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.masterSelected = this.usersList.every(function(item:any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();    
  }

  getCheckedItemList() {
    this.usersSelectedList = this.usersList.filter(el => el.isSelected);
  }
}
